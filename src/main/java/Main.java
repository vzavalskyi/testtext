import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class Main {

    public static void main(String[] args) {
        System.out.println("Input text here: ");
        Scanner in = new Scanner(System.in);
        String text = in.nextLine().toUpperCase();
        System.out.println("Your text is: " + text);
        in.close();
        Map<Boolean, Set<Character>> evenOddSequences = TextTask.splitTextInCharacterSequences(text);
        Set<Character> evenCharacters = evenOddSequences.get(true);
        Set<Character> oddCharacters = evenOddSequences.get(false);
        Set<Integer> asciiEven = TextTask.toAsciiSequence(evenCharacters);
        Set<Integer> asciiOdd = TextTask.toAsciiSequence(oddCharacters);
        int evenCodeSum = TextTask.sumAsciiCode(asciiEven);
        int oddCodeSum = TextTask.sumAsciiCode(asciiOdd);
        System.out.println("Even characters: " + evenCharacters);
        System.out.println("Even Codes: " + asciiEven);
        System.out.println("Even code sum: " + evenCodeSum);
        System.out.println("Odd characters: " + oddCharacters);
        System.out.println("Odd Codes: " + asciiOdd);
        System.out.println("Odd code sum: " + oddCodeSum);
        System.out.println("Sum difference: " + Math.abs(evenCodeSum - oddCodeSum));
    }
}
