import java.util.*;
import java.util.stream.Collectors;

public class TextTask {

    static Map<Boolean, Set<Character>> splitTextInCharacterSequences(String text) {
        Set<Character> symbEven = new HashSet<>();
        Set<Character> symbOdd = new HashSet<>();

        for (char ch : text.toCharArray()) {
            if (ch % 2 == 0) {
                symbEven.add(ch);
            } else {
                symbOdd.add(ch);
            }
        }
        Map<Boolean, Set<Character>> result = new HashMap<>();
        result.put(true, symbEven);
        result.put(false, symbOdd);

        return result;
    }

    static Set<Integer> toAsciiSequence(Set<Character> charSet) {
        return charSet.stream().map(ch -> (int) ch).collect(Collectors.toSet());
    }

    static Integer sumAsciiCode(Set<Integer> codeSet) {
        return codeSet.stream().mapToInt(Integer::intValue).sum();
    }
}
