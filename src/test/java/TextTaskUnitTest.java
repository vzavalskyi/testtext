import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class TextTaskUnitTest {

    @Test
    public void splitTextInCharacterSequencesReturnsEmptyEvenSequence() {
        String text = "Mask";
        Set<Character> evenChars = TextTask.splitTextInCharacterSequences(text).get(true);
        Set<Character> oddChars = TextTask.splitTextInCharacterSequences(text).get(false);
        Assert.assertTrue(evenChars.isEmpty());
        Assert.assertFalse(oddChars.isEmpty());
    }

    @Test
    public void splitTextInCharacterSequencesSpreadsCharactersEquallyBetweenSequences() {
        String text = "abcdef";
        Set<Character> evenChars = TextTask.splitTextInCharacterSequences(text).get(true);
        Set<Character> oddChars = TextTask.splitTextInCharacterSequences(text).get(false);
        Assert.assertEquals(evenChars.size(), oddChars.size());
    }

    @Test
    public void toAsciiSequenceMakesCorrectCodesOnly() {
        Set<Character> chars = new HashSet<>(Arrays.asList('X', 'Y', 'Z'));
        Set<Integer> codes = TextTask.toAsciiSequence(chars);
        Assert.assertFalse(codes.contains(87));
        Assert.assertTrue(codes.contains(88));
        Assert.assertTrue(codes.contains(89));
        Assert.assertTrue(codes.contains(90));
    }

    @Test
    public void sumAsciiCode() {
        Set<Integer> codes = new HashSet<>(Arrays.asList(69, 14, 88));
        Integer expectedSum = 171;
        Integer actualSum = TextTask.sumAsciiCode(codes);
        Assert.assertEquals(expectedSum, actualSum);
    }
}